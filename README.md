# utils-config-env

O módulo `utils-config-env` é uma biblioteca Java projetada para facilitar a gestão de configurações em diferentes ambientes de execução para projetos de automação de testes. Ele permite que as configurações sejam carregadas dinamicamente de arquivos YAML específicos do ambiente.

## Funcionalidades

- Carga dinâmica de configurações de ambiente a partir de arquivos YAML.
- Suporte a múltiplos ambientes como dev, uat e stress.
- Configurações personalizadas para URLs de serviços, conexões de bancos de dados múltiplos (Oracle, MongoDB) e serviços de mensageria Kafka.
- Flexibilidade para incluir ou excluir configurações específicas baseadas na necessidade do projeto.

## Pré-requisitos

- Java JDK 11 ou superior.
- Maven para gerenciamento de dependências e build.

## Instalação

Clone este repositório ou baixe o código fonte diretamente:

```bash
git clone https://gitlab.com/developercmatos/utils-config-env.git
cd utils-config-env
