package env;

import enums.Environment;
import properties.SystemProperties;

public class EnvironmentSelector {

    private final SystemProperties systemProperties;

    public EnvironmentSelector(SystemProperties systemProperties) {
        this.systemProperties = systemProperties;
    }

    public  Environment getEnvironment() {
        String envKey = systemProperties.getProperty("env");
        return Environment.fromString(envKey);
    }
}
