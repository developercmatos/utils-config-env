package config;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class EnvironmentConfigLoader {

    private Map<String, Object> config;

    public EnvironmentConfigLoader(String environment, InputStream inputStream) {
        try {
            if (inputStream == null) {
                inputStream = getClass().getClassLoader().getResourceAsStream("application-" + environment + ".yml");
            }
            Yaml yaml = new Yaml(new Constructor(Map.class));
            config = yaml.load(inputStream);
        } catch (Exception e) {
            throw new RuntimeException("Falha ao carregar configurações do ambiente: " + environment, e);
        }
    }

    public Map<String, Object> getConfig() {
        return config;
    }

    public List<Map<String, String>> getOracleDatabases() {
        return (List<Map<String, String>>) ((Map<String, Object>) config.get("databases")).get("oracle");
    }

    public List<Map<String, String>> getMongoDatabases() {
        return (List<Map<String, String>>) ((Map<String, Object>) config.get("databases")).get("mongodb");
    }

}
