package properties;

public interface SystemProperties {
    String getProperty(String key);
}
