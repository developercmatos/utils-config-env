package properties;

public class DefaultSystemProperties implements SystemProperties{

    @Override
    public String getProperty(String key) {
        return System.getProperty(key);
    }
}
