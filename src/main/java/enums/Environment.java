package enums;

public enum Environment {

    DEV("dev"),
    UAT("uat"),
    STRESS("stress");

    private final String key;

    Environment(String key) {
        this.key = key;
    }

    public String getKey() {
        return this.key;
    }

    public static Environment fromString(String key) {
        for (Environment env : Environment.values()) {
            if (env.getKey().equalsIgnoreCase(key)) {
                return env;
            }
        }
        return UAT;
    }
}
