package unit.enums;

import enums.Environment;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EnvironmentTest {

    @Test
    public void testEnumKeyValues() {
        assertEquals("dev", Environment.DEV.getKey());
        assertEquals("uat", Environment.UAT.getKey());
        assertEquals("stress", Environment.STRESS.getKey());
    }

    @Test
    public void testFromStringWithKnownKeys() {
        assertEquals(Environment.DEV, Environment.fromString("dev"));
        assertEquals(Environment.UAT, Environment.fromString("uat"));
        assertEquals(Environment.STRESS, Environment.fromString("stress"));
    }

    @Test
    public void testFromStringWithUnknownKey() {
        assertEquals(Environment.UAT, Environment.fromString("production"));
    }

    @Test
    public void testFromStringWithCaseInsensitivity() {
        assertEquals(Environment.DEV, Environment.fromString("DEV"));
        assertEquals(Environment.STRESS, Environment.fromString("StReSs"));
    }

    @Test
    public void testFromStringWithNullKey() {
        assertEquals(Environment.UAT, Environment.fromString(null));
    }

}