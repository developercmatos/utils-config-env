package unit.env;

import enums.Environment;
import env.EnvironmentSelector;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import properties.SystemProperties;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
class EnvironmentSelectorTest {
    @Mock
    private SystemProperties mockProperties;

    @ParameterizedTest
    @MethodSource("providers.EnvironmentProvider#provideEnvironmentArguments")
    void testGetEnvironment(String envKey, Environment expectedEnvironment) {
        when(mockProperties.getProperty("env")).thenReturn(envKey);
        EnvironmentSelector selector = new EnvironmentSelector(mockProperties);
        assertEquals(expectedEnvironment, selector.getEnvironment());
    }


    @Test
    void testGetEnvironmentWithUndefinedSystemProperty() {
        when(mockProperties.getProperty("env")).thenReturn(null);
        EnvironmentSelector selector = new EnvironmentSelector(mockProperties);
        assertEquals(Environment.UAT, selector.getEnvironment());
        }


    @Test
    void testGetEnvironmentWithNonexistentValue() {
        when(mockProperties.getProperty("env")).thenReturn("nonexistent");
        EnvironmentSelector selector = new EnvironmentSelector(mockProperties);
            assertEquals(Environment.UAT, selector.getEnvironment());
        }
    }

