package providers;

import enums.Environment;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class EnvironmentProvider {
        public static Stream<Arguments> provideEnvironmentArguments() {
            return Stream.of(
                    Arguments.of("dev", Environment.DEV),
                    Arguments.of("uat", Environment.UAT),
                    Arguments.of("stress", Environment.STRESS)
            );
        }
    }

