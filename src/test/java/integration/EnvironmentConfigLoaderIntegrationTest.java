package integration;

import config.EnvironmentConfigLoader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class EnvironmentConfigLoaderIntegrationTest {


    private EnvironmentConfigLoader configLoader;

    @BeforeEach
    void setup() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application-test.yml");
        configLoader = new EnvironmentConfigLoader("test", inputStream);
    }

    @Test
    void testLoadConfigSuccessfully() {
        assertNotNull(configLoader.getConfig(), "Config should not be null");
        assertEquals("http://test-server:8081", ((Map<String, String>)configLoader.getConfig().get("baseUrls")).get("serviceOne"));
        assertEquals("http://test-server:8082", ((Map<String, String>)configLoader.getConfig().get("baseUrls")).get("serviceTwo"));
    }

    @Test
    void testOracleDatabaseConfigurations() {
        List<Map<String, String>> oracleDbs = configLoader.getOracleDatabases();
        assertNotNull(oracleDbs, "Oracle database configurations should not be null");
        assertFalse(oracleDbs.isEmpty(), "Oracle database list should not be empty");
        assertEquals("jdbc:oracle:thin:@test-db:1521:xe", oracleDbs.get(0).get("url"));
        assertEquals("testuser", oracleDbs.get(0).get("user"));
        assertEquals("testpass", oracleDbs.get(0).get("password"));
    }

    @Test
    void testMongoDatabaseConfigurations() {
        List<Map<String, String>> mongoDbs = configLoader.getMongoDatabases();
        assertNotNull(mongoDbs, "MongoDB configurations should not be null");
        assertFalse(mongoDbs.isEmpty(), "MongoDB list should not be empty");
        assertEquals("mongodb://test-mongo:27017", mongoDbs.get(0).get("url"));
        assertEquals("testDB", mongoDbs.get(0).get("dbName"));
    }

    @Test
    void testKafkaConfigurations() {
        Map<String, String> kafkaConfig = (Map<String, String>) configLoader.getConfig().get("kafka");
        assertNotNull(kafkaConfig, "Kafka configuration should not be null");
        assertEquals("test-kafka:9092", kafkaConfig.get("bootstrapServers"));
    }
}

